variable "app_name" {
  description = "Name of the application"
}
variable "env_name" {
    description = "name of the environment eg: dev, staging, production."
}
variable "cost_center" {
    description = "Name of the application's which will be used to identify the infrastructure cost"
}