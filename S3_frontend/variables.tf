variable "app_name" {
  description = "Name of the project"
}
variable "env_name" {
    description = "name of the environment eg: dev, staging, production."
}
variable "cost_center" {
    description = "cost center for the task"
}