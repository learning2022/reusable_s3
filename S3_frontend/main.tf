resource "aws_s3_bucket" "s3_frontend" {
  bucket = "${var.app_name}-${var.env_name}-static-contents"

  tags = {
    "Name"        = "${var.app_name}-${var.env_name}-static-contents"
    "Environment" = "${var.app_name}-${var.env_name}"
    "Application" = var.app_name
    "CostCenter"  = var.cost_center
  }
}
resource "aws_s3_bucket_acl" "s3_frontend_acl" {
  bucket = aws_s3_bucket.s3_frontend.id
  acl = "private"
}
resource "aws_s3_bucket_versioning" "s3_frontend_versioning" {
  bucket = aws_s3_bucket.s3_frontend.id
  versioning_configuration {
    status = "Enabled"
    # mfa_delete = "Enabled"
  }
}
resource "aws_s3_bucket_public_access_block" "s3_frontend_block_public_access" {
  bucket = aws_s3_bucket.s3_frontend.id
  block_public_acls   = true
  block_public_policy = true
}
resource "aws_kms_key" "kms_key" {
  description             = "This key is used to encrypt bucket objects"
  deletion_window_in_days = 10
}
resource "aws_s3_bucket_server_side_encryption_configuration" "s3_frontend_server_side_encryption" {
  bucket = aws_s3_bucket.s3_frontend.id

  rule {
    apply_server_side_encryption_by_default {
      kms_master_key_id = aws_kms_key.kms_key.arn
      sse_algorithm     = "aws:kms"
    }
  }
}
resource "aws_s3_bucket_policy" "s3_frontend_request_https" {
    bucket = aws_s3_bucket.s3_frontend.id

    policy = jsonencode({
        Version = "2012-10-17"
        Id      = "BUCKET-POLICY"
        Statement = [
            {
                Sid       = "EnforceTls"
                Effect    = "Deny"
                Principal = "*"
                Action    = "s3:*"
                Resource = [
                    "${aws_s3_bucket.frontend_bucket_for_reuse.arn}/*",
                    "${aws_s3_bucket.frontend_bucket_for_reuse.arn}",
                ]
                Condition = {
                    Bool = {
                        "aws:SecureTransport" = "false"
                    }
                }
            },
        ]
    })
}
resource "aws_s3_bucket_website_configuration" "s3_frontend_website_configuration" {
  bucket = aws_s3_bucket.s3_frontend.id

  index_document {
    suffix = "index.html"
  }

  error_document {
    key = "error.html"
  }

  routing_rule {
    condition {
      key_prefix_equals = "docs/"
    }
    redirect {
      replace_key_prefix_with = "documents/"
    }
  }
}