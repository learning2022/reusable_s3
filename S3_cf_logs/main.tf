resource "aws_s3_bucket" "cf_logs" {
  bucket = "${var.app_name}-${var.env_name}-cf-logs"

  tags = {
    "Name"        = "${var.app_name}-${var.env_name}-cf-logs"
    "Environment" = "${var.app_name}-${var.env_name}"
    "Application" = var.app_name
    "CostCenter"  = var.cost_center
  }
}
resource "aws_s3_bucket_acl" "cf_logs_acl" {
  bucket = aws_s3_bucket.cf_logs.id
  acl = "private"
}
resource "aws_s3_bucket_versioning" "cf_logs_versioning" {
  bucket = aws_s3_bucket.cf_logs.id
  versioning_configuration {
    status = "Enabled"
    # mfa_delete = "Enabled"
  }
}
resource "aws_s3_bucket_public_access_block" "cf_logs_block_public_access" {
  bucket = aws_s3_bucket.cf_logs.id
  block_public_acls   = true
  block_public_policy = true
}
resource "aws_kms_key" "cf_logs_kms_key" {
  description             = "This key is used to encrypt bucket objects"
  deletion_window_in_days = 10
}
resource "aws_s3_bucket_server_side_encryption_configuration" "cf_logs_server_side_encryption" {
  bucket = aws_s3_bucket.cf_logs.id

  rule {
    apply_server_side_encryption_by_default {
      kms_master_key_id = aws_kms_key.cf_logs_kms_key.arn
      sse_algorithm     = "aws:kms"
    }
  }
}
resource "aws_s3_bucket_policy" "cf_logs_request_https" {
    bucket = aws_s3_bucket.cf_logs.id

    policy = jsonencode({
        Version = "2012-10-17"
        Id      = "BUCKET-POLICY"
        Statement = [
            {
                Sid       = "EnforceTls"
                Effect    = "Deny"
                Principal = "*"
                Action    = "s3:*"
                Resource = [
                    "${aws_s3_bucket.test_bucket_for_reuse.arn}/*",
                    "${aws_s3_bucket.test_bucket_for_reuse.arn}",
                ]
                Condition = {
                    Bool = {
                        "aws:SecureTransport" = "false"
                    }
                }
            },
        ]
    })
}